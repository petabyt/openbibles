const CaV = require('chapter-and-verse');
const btl = require('bible-translation-lookup');
const books = require('bible-book-num');

/**
     * Retrieve bible passages
     *
     * @param {String} ref - bible book/chapter/verse
     * @param {String} ver - bible version
     * @return {String} returns String passage
     */
module.exports = function (ref, ver, htmlVerse) {
    if (typeof ref !== 'string') {
        throw new TypeError('Passage should be a string');
    }
    if (typeof ver !== 'undefined' && typeof ver !== 'string') {
        throw new TypeError('Version should be a string');
    }

    // Separate language from version
    let lang = ver.split('-')[0];
    if (lang == ver) {
        lang = 'en';
    }

    let v;
    try {
        v = btl(ver.split('-')[1]).abbr.toLowerCase('');
    }
    catch(error) {
        try {
            v = ver.split('-')[1].toLowerCase('');
        } catch (error) {
            v = ver.toLowerCase('');
        }
    }

    let a = CaV(ref);
    let id = a.book.id;
    let chapter = a.chapter;
    let startverse = a.from;
    let bible;
    let singleChapter;
    if (a.book.versesPerChapter.length == 1) {
        singleChapter = true;
    } else {
        singleChapter = false;
    }
    
    try {
        bible = require('./bibles/' + lang + '/' + v);
    }
    catch(error) {
        throw new Error(error);
        try {
            bible = require('./bibles/en/' + v);
        } catch (error) {
            throw new Error(error);
        }
    }

    let text = '';
    // If htmlVerse is true it gives some basic HTML syntax to the numbers. It is built for Heb12 Desktop
    if (htmlVerse == true) {
        if (a.from < a.to) {
            for (var i = a.from; i < a.to + 1; i++) {
                let rawText;
                if (a.book.versesPerChapter.length == 1) {
                    rawText = bible.osis.osisText.div[books(id)].chapter.verse[i - 1].text + ' ';
                } else {
                    rawText = bible.osis.osisText.div[books(id)].chapter[chapter - 1].verse[i - 1].text + ' ';
                }
                text = text + '<p class="verse">' + '<b class="vref"' + ' onclick="openVerse(\'' + a.book.name + ' ' + a.chapter + ':' + i + '\')"' + '>' + i + '</b> ' + ' ' + rawText;
            }
        } else if (a.from !== null) {
            let rawText;
            if (a.book.versesPerChapter.length == 1) {
                rawText = bible.osis.osisText.div[books(id)].chapter.verse[startverse - 1].text;
            } else {
                rawText = bible.osis.osisText.div[books(id)].chapter[chapter - 1].verse[startverse - 1].text;
            }
            text = rawText;
        } else {
            for (var i = 1; i < a.book.versesPerChapter[chapter - 1] + 1; i++) {
                let rawText;
                if (a.book.versesPerChapter.length == 1) {
                    rawText = bible.osis.osisText.div[books(id)].chapter.verse[i - 1].text + ' ';
                } else {
                    rawText = bible.osis.osisText.div[books(id)].chapter[chapter - 1].verse[i - 1].text + ' ';
                }
                text = text + '<p class="verse">' + '<b class="vref"' + ' onclick="openVerse(\'' + a.book.name + ' ' + a.chapter + ':' + i + '\')"' + '>' + i + '</b> ' + ' ' + rawText;
            }
        }
    } else {
        if (a.from < a.to) {
            for (var i = a.from; i < a.to + 1; i++) {
                let rawText;
                if (a.book.versesPerChapter.length == 1) {
                    rawText = bible.osis.osisText.div[books(id)].chapter.verse[i - 1].text + ' ';
                } else {
                    rawText = bible.osis.osisText.div[books(id)].chapter[chapter - 1].verse[i - 1].text + ' ';
                }
                text = text + i + ' ' + rawText;
            }
        } else if (a.from !== null) {
            let rawText;
            if (a.book.versesPerChapter.length == 1) {
                rawText = bible.osis.osisText.div[books(id)].chapter.verse[startverse - 1].text;
            } else {
                rawText = bible.osis.osisText.div[books(id)].chapter[chapter - 1].verse[startverse - 1].text;
            }
            text = rawText;
        } else {
            for (var i = 1; i < a.book.versesPerChapter[chapter - 1] + 1; i++) {
                let rawText;
                if (a.book.versesPerChapter.length == 1) {
                    rawText = bible.osis.osisText.div[books(id)].chapter.verse[i - 1].text + ' ';
                } else {
                    rawText = bible.osis.osisText.div[books(id)].chapter[chapter - 1].verse[i - 1].text + ' ';
                }
                text = text + i + ' ' + rawText;
            }
        }
    }


    return text;
}
